#!/bin/bash

mkdir -p /usr/include/rpc

for ii in $(cd /usr/include/tirpc/ && ls *.h); do
  ln -s /usr/include/tirpc/$ii /usr/include/rpc/$ii
done

for ii in $(cd /usr/include/tirpc/rpc/ && ls *.h); do
  ln -s /usr/include/tirpc/rpc/$ii /usr/include/rpc/$ii
done

ln -s /usr/include/tirpc/netconfig.h /usr/include/netconfig.h

